﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using BjSTools.MultiMedia;
using ExtractDNAfromVideos.Models;


namespace ExtractDNAfromVideos
{
    public partial class Form1 : Form
    {
        private string filename;
        private string imgfilename;

        public Form1()
        {
            InitializeComponent();
        }

        public Form1(Video vdo, string pathName)
        {
            // TODO: Complete member initialization
            InitializeComponent();
            this.filename = pathName;

            //이미지 업데이트
            //동영상 업데이트
            
            //dna 추출
            this.Show();
            extractDNA(vdo, pathName);
          
            this.Close();
        }
        private void extractDNA(Video thisVideo, string pathName)
        {
            
                string videofilename = pathName;
                MAMDataContext db = new MAMDataContext();

                VDNA dbVDNA = null;

                List<vDNA> vDNAs = new List<vDNA>();
                vDNA vDNA = null;
                vDNA vDNAref = null;
                int diff = 0;
//                int cnt = 0;
          
                {
                    {
                        videofilename = /*path +*/ pathName;
                        FFmpegMediaInfo info = new FFmpegMediaInfo(videofilename, label1);
                        double length = info.Duration.TotalSeconds;
                        //double step = 1; //1초마다
                        //double pos = 1.0; // 첫프레임은 빼고 1초 후부터 (첫프레임이 검거나 유사한 게 많음)
                        int idx = 1;
                        float r11, r12, r21, r22;
                        float g11, g12, g21, g22;
                        float b11, b12, b21, b22;
                        long height = 0;
                        long width = 0;
                        float hw = 0;
                        // 화면 캡쳐 Nframes 단위로
                        info.GetSnapshotAll();

                        byte[] fileData;
                        Bitmap bmp;
                        int num_frames = 1;
                        string strFrameNumber = num_frames.ToString();
                        string filename = "c:\\tmp\\snapshots\\outs-"
                            + "00000".Substring(0, 5 - strFrameNumber.Length) + strFrameNumber
                            + ".jpg";

                        //Dictionary<TimeSpan, Bitmap> snapshots = new Dictionary<TimeSpan, Bitmap>();
                        while (File.Exists(filename))
                        {
                            fileData = File.ReadAllBytes(filename);
                            bmp = new Bitmap(new MemoryStream(fileData));
                            File.Delete(filename);

                            if (bmp != null)
                            {
                                pictureBox1.Image = bmp;
                                pictureBox1.Refresh();
                                height = bmp.Height;
                                width = bmp.Width;

                                vDNA = new vDNA();
                                vDNA.idx = idx;

                                r11 = 0; r12 = 0; r21 = 0; r22 = 0;
                                g11 = 0; g12 = 0; g21 = 0; g22 = 0;
                                b11 = 0; b12 = 0; b21 = 0; b22 = 0;
                                for (int i = 0; i < height - 1; i++)
                                {
                                    for (int j = 0; j < width - 1; j++)
                                    {
                                        if (i < (height / 2) && j < (width / 2))
                                        {
                                            r11 += (int)bmp.GetPixel(j, i).R;
                                            g11 += (int)bmp.GetPixel(j, i).G;
                                            b11 += (int)bmp.GetPixel(j, i).B;
                                        }
                                        else if (i < (height / 2) && j >= (width / 2))
                                        {
                                            r12 += (int)bmp.GetPixel(j, i).R;
                                            g12 += (int)bmp.GetPixel(j, i).G;
                                            b12 += (int)bmp.GetPixel(j, i).B;
                                        }
                                        else if (i >= (height / 2) && j < (width / 2))
                                        {
                                            r21 += (int)bmp.GetPixel(j, i).R;
                                            g21 += (int)bmp.GetPixel(j, i).G;
                                            b21 += (int)bmp.GetPixel(j, i).B;
                                        }
                                        else
                                        {
                                            r22 += (int)bmp.GetPixel(j, i).R;
                                            g22 += (int)bmp.GetPixel(j, i).G;
                                            b22 += (int)bmp.GetPixel(j, i).B;
                                        }
                                    }
                                }
                                bmp = null;
                                hw = height * width / (float)4.0;
                                r11 /= hw; g11 /= hw; b11 /= hw;
                                r21 /= hw; g21 /= hw; b21 /= hw;
                                r12 /= hw; g12 /= hw; b12 /= hw;
                                r22 /= hw; g22 /= hw; b22 /= hw;

                                //snapshots[position] = bmp;
                                Debug.WriteLine(idx + ": " + r11 + "," + g11 + "," + b11 + " vs " + r12 + "," + g12 + "," + b12 + " vs " + r21 + "," + g21 + "," + b21 + " vs " + r22 + "," + g22 + "," + b22);

                                // byte레벨로 소수점 이하가 사라짐. 오차 발생 포인트
                                vDNA.r1 = (byte)r11; vDNA.r2 = (byte)r12; vDNA.r3 = (byte)r21; vDNA.r4 = (byte)r22;
                                vDNA.g1 = (byte)g11; vDNA.g2 = (byte)g12; vDNA.g3 = (byte)g21; vDNA.g4 = (byte)g22;
                                vDNA.b1 = (byte)b11; vDNA.b2 = (byte)b12; vDNA.b3 = (byte)b21; vDNA.b4 = (byte)b22;
                                // diff 계산: 11프레임부터 계산
                                for (int i = idx - 1; i > 0 && ((idx - i) < 5); i--)
                                {
                                    vDNAref = vDNAs.Where(v => v.idx == i).FirstOrDefault();
                                    if (vDNAref != null)
                                    {
                                        diff = 0;
                                        diff = Math.Abs(vDNA.r1 - vDNAref.r1);
                                        diff += Math.Abs(vDNA.r2 - vDNAref.r2);
                                        diff += Math.Abs(vDNA.r3 - vDNAref.r3);
                                        diff += Math.Abs(vDNA.r4 - vDNAref.r4);
                                        diff += Math.Abs(vDNA.g1 - vDNAref.g1);
                                        diff += Math.Abs(vDNA.g2 - vDNAref.g2);
                                        diff += Math.Abs(vDNA.g3 - vDNAref.g3);
                                        diff += Math.Abs(vDNA.g4 - vDNAref.g4);
                                        diff += Math.Abs(vDNA.b1 - vDNAref.b1);
                                        diff += Math.Abs(vDNA.b2 - vDNAref.b2);
                                        diff += Math.Abs(vDNA.b3 - vDNAref.b3);
                                        diff += Math.Abs(vDNA.b4 - vDNAref.b4);

                                        switch ((int)idx - i)
                                        {
                                            case 1: vDNAref.diff1 = diff;
                                                break;
                                            case 2: vDNAref.diff2 = diff;
                                                break;
                                            case 3: vDNAref.diff3 = diff;
                                                break;
                                            case 4: vDNAref.diff4 = diff;
                                                break;
                                            //case 5: vDNAref.diff5 = diff;
                                            //    break;
                                            //case 6: vDNAref.diff6 = diff;
                                            //    break;
                                            //case 7: vDNAref.diff7 = diff;
                                            //    break;
                                            //case 8: vDNAref.diff8 = diff;
                                            //    break;
                                            //case 9: vDNAref.diff9 = diff;
                                            //    break;
                                        }
                                    }
                                }
                                // 추가
                                vDNAs.Add(vDNA);
                                idx++;
                            }
                            strFrameNumber = (++num_frames).ToString();
                            filename = "c:\\tmp\\snapshots\\outs-"
                                + "00000".Substring(0, 5 - strFrameNumber.Length) + strFrameNumber
                                + ".jpg";

                            // refresh screen
                            this.Refresh();
                            Application.DoEvents();
                        }
                        //FFMPEG 릴리즈
                        info = null;

                        // DB Insertion: diff가 Null인지 확인 필요.
                        foreach (vDNA aDNA in vDNAs)
                        {
                            dbVDNA = new VDNA(); //DB객체
                            dbVDNA.r1 = aDNA.r1; dbVDNA.r2 = aDNA.r2; dbVDNA.r3 = aDNA.r3; dbVDNA.r4 = aDNA.r4;
                            dbVDNA.g1 = aDNA.g1; dbVDNA.g2 = aDNA.g2; dbVDNA.g3 = aDNA.g3; dbVDNA.g4 = aDNA.g4;
                            dbVDNA.b1 = aDNA.b1; dbVDNA.b2 = aDNA.b2; dbVDNA.b3 = aDNA.b3; dbVDNA.b4 = aDNA.b4;
                            if (aDNA.diff1 != null) dbVDNA.diff1 = (Int16)aDNA.diff1;
                            if (aDNA.diff2 != null) dbVDNA.diff2 = (Int16)aDNA.diff2;
                            if (aDNA.diff3 != null) dbVDNA.diff3 = (Int16)aDNA.diff3;
                            if (aDNA.diff4 != null) dbVDNA.diff4 = (Int16)aDNA.diff4;
                                                    //if (aDNA.diff5 != null) dbVDNA.diff5 = (Int16)aDNA.diff5;
                                                    //if (aDNA.diff6 != null) dbVDNA.diff6 = (Int16)aDNA.diff6;
                                                    //if (aDNA.diff7 != null) dbVDNA.diff7 = (Int16)aDNA.diff7;
                                                    //if (aDNA.diff8 != null) dbVDNA.diff8 = (Int16)aDNA.diff8;
                                                    //if (aDNA.diff9 != null) dbVDNA.diff9 = (Int16)aDNA.diff9;
                            dbVDNA.idx = aDNA.idx;

                            thisVideo.VDNA.Add(dbVDNA);
                            db.VDNA.InsertOnSubmit(dbVDNA);
                        }
                        // 동영상 하나씩은 저장함.
                        vDNAs = null;

                    }

                }

                Debug.WriteLine("End===");
                Debug.Flush();
          
        }

        private static void print(Bitmap BM, PaintEventArgs e)
        {
            Graphics graphicsObj = e.Graphics; // or "Bitmap bitmap = new Bitmap("Grapes.jpg");"
            graphicsObj.DrawImage(BM, 60, 10); // or "e.Graphics.DrawImage(bitmap, 60, 10);"
            graphicsObj.Dispose();
        }

    }
}
