﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExtractDNAfromVideos
{
    public partial class Form2 : Form
    {
        public string ReturnImgFilename = "";
        public string ReturnTitle = "";
        public string ReturnOverview= "";

        public Form2()
        {
            InitializeComponent();
        }
        public Form2(string filename)
        {
            InitializeComponent();
            this.labelFilename.Text = filename;
            
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            this.ReturnImgFilename=labelImgfilename.Text;
            this.ReturnTitle=textBox2.Text;
            this.ReturnOverview = textBox1.Text;

            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string filename = "";
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "video files (*.mp4)|*.mp4|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    filename = openFileDialog1.FileName;

                    if (!string.IsNullOrEmpty(filename))
                    {
                        labelImgfilename.Text = filename;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

    }
}
