﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
//using System.Drawing;
using BjSTools.MultiMedia;
using ExtractDNAfromVideos.Models;
using System.Collections.Specialized;
using System.Data.SqlClient;


namespace ExtractDNAfromVideos
{
    public partial class Form3 : Form
    {

        private string[] Filenames;
        private string filename = "";
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
         
           
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "video files (*.mp4)|*.mp4|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    
                    filename = openFileDialog1.FileName;
                    
                    if (!string.IsNullOrEmpty(filename))
                    {
                        openMetaDialog(filename);
                              
                        MessageBox.Show("폴더에 있는 동영상 파일 하나 열기: " + filename);

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }


        }

        private static void print(Bitmap BM, PaintEventArgs e)
        {
            Graphics graphicsObj = e.Graphics; // or "Bitmap bitmap = new Bitmap("Grapes.jpg");"
            graphicsObj.DrawImage(BM, 60, 10); // or "e.Graphics.DrawImage(bitmap, 60, 10);"
            graphicsObj.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dlg = new FolderBrowserDialog())
            {
                dlg.Description = "Select a folder";
                if (dlg.ShowDialog() == DialogResult.OK)
                {

                    MessageBox.Show("You selected: " + dlg.SelectedPath);

                    string path = dlg.SelectedPath;

                    Filenames = Directory.GetFiles(path, "*.mp4");//확장자 추가해야됨
                    foreach(string fn in Filenames)
                    {
                        //MessageBox.Show("폴더에 있는 동영상 파일 하나 열기: " + filename);
                        openMetaDialog(fn);
                    }
            
                   
                }
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            this.dataGridView1.ColumnCount = 4;
            this.dataGridView1.Columns[0].Name = "Title";
            this.dataGridView1.Columns[1].Name = "VideoFileName";
            this.dataGridView1.Columns[2].Name = "ImageFileName";
            this.dataGridView1.Columns[3].Name = "Overview";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            System.Net.WebClient webClient = new System.Net.WebClient();
            MAMDataContext db = new MAMDataContext();
            Video thisVideo = null;

            string uniqname = "", uniqFilename = "", uniqImgname = "";
            string filename = "", imgname = "";

            //grid data 수만큼 반복
            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                //1. 메타 데이터 적재
                thisVideo = new Video();
                filename = (string)dataGridView1.Rows[i].Cells[1].Value;
                imgname = (string)dataGridView1.Rows[i].Cells[2].Value;
                //0. 서버에 동영상 저장
                uniqname = DateTime.UtcNow.Ticks.ToString();
                uniqFilename = "strVid_" + uniqname + "." + Path.GetFileName(filename);/*uniq string*/
                uniqImgname = "strImg_" + uniqname + "." + Path.GetFileName(imgname);

                NameValueCollection parameters = new NameValueCollection();
                parameters.Add("uName", uniqFilename);
                webClient.QueryString = parameters;
                try {
                var responseBytes = webClient.UploadFile(@"http://localhost:8008/upload.aspx", filename);
                string response = Encoding.ASCII.GetString(responseBytes);

                parameters.Clear();
                responseBytes = null;
                response = "";

                parameters.Add("uName", uniqImgname);
                webClient.QueryString = parameters;
                responseBytes = webClient.UploadFile(@"http://localhost:8008/upload.aspx", imgname);
                response = Encoding.ASCII.GetString(responseBytes);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
            }

            try
                {
                    thisVideo.title = (string)dataGridView1.Rows[i].Cells[0].Value;
                    thisVideo.overview = (string)dataGridView1.Rows[i].Cells[3].Value;
                    //db 수정 필요
                    thisVideo.filename = uniqFilename;
                    thisVideo.imgname = /*(string)dataGridView1.Rows[i].Cells[2].Value*/ uniqImgname;

                    db.Video.InsertOnSubmit(thisVideo);


                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
                
                //2. 폼1 콜
                MessageBox.Show(/*"야홋: " +*/dataGridView1.Rows[i].Cells[1].Value.ToString());
                openExtractor(thisVideo, filename);
                db.SubmitChanges();
                thisVideo = null;
                    //2.1 (폼1에서) upload img
                    //2.2 (폼1에서) upload vid
                    //2.3 (폼1에서) dna 추출
                    //2.3 (폼1에서) dna db에 적재
                
            }
            
            //grid 비우기
            do
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    try
                    {
                        dataGridView1.Rows.Remove(row);
                    }
                    catch (Exception) { }
                }
            } while (dataGridView1.Rows.Count > 1);
        }


        private void openExtractor(Video vdo, string pathName)
        {
            Form1 form1 = new Form1(vdo, pathName);
          
         
        }

        private void openMetaDialog(string filename)
        {
            Form2 form2 = new Form2(filename);

                  var result = form2.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            string title = form2.ReturnTitle;
                            string imgfilename = form2.ReturnImgFilename;
                            string overview = form2.ReturnOverview;
                            
                            dataGridView1.Rows.Add(title,filename,imgfilename,overview);
                        }
                        
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }


    }
}
