RTVS Fingerprint Extractor README
=============

RTVS Fingerprint Extractor shows how to extract fingerprints from specific video file.

## Libraries


## Tools


## Documentation

The offline documentation is available in the **doc/** directory.

### Examples

Coding examples are available in the **doc/examples** directory.

## License

RTVS Fingerprint Extractor is mainly LGPL-licensed with optional components licensed under
GPL. Please refer to the LICENSE file for detailed information.