﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtractDNAfromVideos.Models
{
    public class vDNA
    {
        public int idx { get; set; }
        public byte r1 { get; set; }
        public byte g1 { get; set; }
        public byte b1 { get; set; }
        public byte r2 { get; set; }
        public byte g2 { get; set; }
        public byte b2 { get; set; }
        public byte r3 { get; set; }
        public byte g3 { get; set; }
        public byte b3 { get; set; }
        public byte r4 { get; set; }
        public byte g4 { get; set; }
        public byte b4 { get; set; }
        public byte r5 { get; set; }
        public byte g5 { get; set; }
        public byte b5 { get; set; }
        public byte r6 { get; set; }
        public byte g6 { get; set; }
        public byte b6 { get; set; }
        public byte r7 { get; set; }
        public byte g7 { get; set; }
        public byte b7 { get; set; }
        public byte r8 { get; set; }
        public byte g8 { get; set; }
        public byte b8 { get; set; }
        public byte r9 { get; set; }
        public byte g9 { get; set; }
        public byte b9 { get; set; }
        public int? diff1 { get; set; }
        public int? diff2 { get; set; }
        public int? diff3 { get; set; }
        public int? diff4 { get; set; }

    }
}
