﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;

namespace BjSTools.MultiMedia
{
    public class FFmpegMediaInfo
    {
        private static string FFMPEG_EXE_PATH = "c:\\RTVS\\ffmpeg\\ffmpeg.exe"; ///CheckRelativePath("c:\\ffmpeg\ffmpeg.exe");
        private static string FFPROBE_EXE_PATH = "c:\\RTVS\\ffmpeg\\ffprobe.exe"; ///CheckRelativePath("c:\\ffmpeg\ffprobe.exe");

        #region static helpers

        /// <summary>
        /// Safely converts a string in format h:m:s.f to a TimeSpan using Regex allowing every part being as long as is
        /// </summary>
        private static TimeSpan ConvertFFmpegTimeSpan(string value)
        {
            Match m = _rexTimeSpan.Match(value);
            double v = 0.0;
            if (m == null || !m.Success) return new TimeSpan();

            if (!String.IsNullOrEmpty(m.Groups["h"].Value))
                v += Convert.ToInt32(m.Groups["h"].Value);
            v *= 60.0;

            if (!String.IsNullOrEmpty(m.Groups["m"].Value))
                v += Convert.ToInt32(m.Groups["m"].Value);
            v *= 60.0;

            if (!String.IsNullOrEmpty(m.Groups["s"].Value))
                v += Convert.ToInt32(m.Groups["s"].Value);

            if (!String.IsNullOrEmpty(m.Groups["f"].Value))
                v += Convert.ToDouble(String.Format("0{1}{0}", m.Groups["f"].Value, CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator));

            return TimeSpan.FromSeconds(v);
        }
        private static readonly Regex _rexTimeSpan = new Regex(@"^(((?<h>\d+):)?(?<m>\d+):)?(?<s>\d+)([\.,](?<f>\d+))?$", RegexOptions.Compiled);

        /// <summary>
        /// Checks if the passed path is rooted and if not resolves it relative to the calling assembly
        /// </summary>
        private static string CheckRelativePath(string path)
        {
            if (!Path.IsPathRooted(path))
            {
                string appDir = Path.GetDirectoryName(Assembly.GetCallingAssembly().GetName().CodeBase);
                path = Path.Combine(appDir, path);
            }

            if (path.StartsWith("file:", StringComparison.OrdinalIgnoreCase))
                path = path.Substring(6);

            return path;
        }

        /// <summary>
        /// Executes a process and passes its command-line output back after the process has exitted
        /// </summary>
        private static string Execute(string exePath, string parameters)
        {
            string result = String.Empty;

            using (Process p = new Process())
            {
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.FileName = exePath;
                p.StartInfo.Arguments = parameters;
                p.Start();
                p.WaitForExit();

                result = p.StandardOutput.ReadToEnd();
            }

            return result;
        }

        #endregion

        #region Properies

        public string Filename { get; set; }
        public TimeSpan Duration { get; set; }
        public string FormatName { get; set; }
        public string FormatNameLong { get; set; }
        public string BitRate { get; set; }

        public List<FFmpegStreamInfo> Streams { get; set; }
        public List<KeyValuePair<string, string>> OtherValues { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }

        #endregion

        public FFmpegMediaInfo()
        {
            Streams = new List<FFmpegStreamInfo>();
            OtherValues = new List<KeyValuePair<string, string>>();
        }
        public FFmpegMediaInfo(string filename, System.Windows.Forms.Label label1)
            : this()
        {
            this.Filename = filename;

            try
            {
                string cmdParams = String.Format("-hide_banner -show_format -show_streams -pretty {1}{0}{1}", filename, filename.Contains(' ') ? "\"" : "");
                string[] lines = Execute(FFPROBE_EXE_PATH, cmdParams).Replace("\r\n", "\n").Replace('\r', '\n').Split('\n');
                int curr = 0;
                FFmpegStreamInfo stream = null;
                StringComparison sc = StringComparison.OrdinalIgnoreCase;
                string name;
                string value;
                foreach (string line in lines)
                {
                    label1.Text += line + "\n";
                    if (line.StartsWith("[/"))
                        curr = 0;
                    else if (line.StartsWith("[FORMAT]"))
                        curr = 1;
                    else if (line.StartsWith("[STREAM]"))
                    {
                        stream = new FFmpegStreamInfo();
                        this.Streams.Add(stream);
                        curr = 2;
                    }
                    else if (curr > 0)
                    {
                        // Split name and value
                        int pos = line.IndexOf('=');
                        if (pos < 1) continue;
                        name = line.Substring(0, pos);
                        value = (pos == line.Length - 1) ? String.Empty : line.Substring(pos + 1);

                        if (curr == 1)
                        {
                            #region FFmpegMediaInfo fields
                            if (name.Equals("format_name", sc))
                                this.FormatName = value;
                            else if (name.Equals("format_long_name", sc))
                                this.FormatNameLong = value;
                            else if (name.Equals("duration", sc))
                                this.Duration = ConvertFFmpegTimeSpan(value);
                            else if (name.Equals("bit_rate", sc))
                                this.BitRate = value;
                            else
                                this.OtherValues.Add(new KeyValuePair<string, string>(name, value));
                            #endregion
                        }
                        else if (curr == 2)
                        {
                            #region FFmpegStreamInfo fields
                            if (name.Equals("index", sc))
                                stream.Index = Convert.ToInt32(value);
                            else if (name.Equals("codec_name", sc))
                                stream.CodecName = value;
                            else if (name.Equals("codec_long_name", sc))
                                stream.CodecNameLong = value;
                            else if (name.Equals("codec_type", sc))
                                stream.CodecType = value;
                            else if (name.Equals("codec_time_base", sc))
                                stream.CodecTimeBase = value;
                            else if (name.Equals("codec_tag_string", sc))
                                stream.CodecTagString = value;
                            else if (name.Equals("codec_tag", sc))
                                stream.CodecTag = value;
                            else if (name.Equals("width", sc))
                                stream.Width = String.IsNullOrEmpty(value) ? 0 : Convert.ToInt32(value);
                            else if (name.Equals("height", sc))
                                stream.Height = String.IsNullOrEmpty(value) ? 0 : Convert.ToInt32(value);
                            else if (name.Equals("sample_aspect_ratio", sc))
                                stream.SampleAspectRatio = value;
                            else if (name.Equals("display_aspect_ratio", sc))
                                stream.DisplayAspectRation = value;
                            else if (name.Equals("pix_fmt", sc))
                                stream.PixelFormat = value;
                            else if (name.Equals("r_frame_rate", sc))
                                stream.FrameRate = value;
                            else if (name.Equals("start_time", sc))
                                stream.StartTime = ConvertFFmpegTimeSpan(value);
                            else if (name.Equals("duration", sc))
                                stream.Duration = ConvertFFmpegTimeSpan(value);
                            else if (name.Equals("nb_frames", sc))
                                stream.FrameCount = String.IsNullOrEmpty(value) ? 0 : Convert.ToInt64(value);
                            else
                                stream.OtherValues.Add(new KeyValuePair<string, string>(name, value));
                            #endregion
                        }
                    }
                }

                // Search the first video stream and copy video size to FFmpegMediaInfo for easier access
                FFmpegStreamInfo video = this.Streams.FirstOrDefault(s => s.CodecType.Equals("video", sc));
                if (video != null)
                {
                    this.Width = video.Width;
                    this.Height = video.Height;
                }
            }
            catch { }

        }

        // 스냅샷을 모두 만드는 로직
        public void GetSnapshotAll()
        {
            const int Nframes = 10;
            string filename = this.Filename;
            if (filename.Contains(' ')) filename = "\"" + filename + "\"";

            // 디렉토리 없을 때는 스냅샷용 폴더를 만듦. --> 권한이 없을 수 있음.
            if (!Directory.Exists("c:\\tmp\\snapshots"))
            {
                Directory.CreateDirectory("c:\\tmp\\snapshots");
            }
            // 1초 뒤에 스냅샷을 Nframes 단위로 저장함.
            string cmdParams = String.Format("-hide_banner -ss 00:00:01 -i {0} -vsync 0 -filter:v \"select=\'between(mod(n, {1}),{2},{2})\'\" c:\\tmp\\snapshots\\outs-%05d.jpg", filename, Nframes.ToString(), (Nframes - 1).ToString());

            try
            {
                Execute(FFMPEG_EXE_PATH, cmdParams);
            }
            catch { }
        }

        public Bitmap GetSnapshot(TimeSpan atPositioin)
        {
            string filename = this.Filename;
            if (filename.Contains(' '))
                filename = "\"" + filename + "\"";

            string tmpFileName = Path.GetTempFileName();
            if (tmpFileName.Contains(' '))
                tmpFileName = "\"" + tmpFileName + "\"";
            tmpFileName = tmpFileName.Replace("tmp", "bmp");

            string cmdParams = String.Format("-hide_banner -ss {0} -i {1} -y -r 1 -t 1 -f image2 {2}", atPositioin, filename, tmpFileName);

            Bitmap result = null;
            try
            {
                Execute(FFMPEG_EXE_PATH, cmdParams);

                if (File.Exists(tmpFileName))
                {
                    // Do not open the Bitmap directly from the file, because then the file is locked until the Bitmap is disposed!
                    byte[] fileData = File.ReadAllBytes(tmpFileName);
                    result = new Bitmap(new MemoryStream(fileData));
                    File.Delete(tmpFileName);
                }
            }
            catch { }

            return result;
        }
    }

    public class FFmpegStreamInfo
    {
        public int Index { get; set; }
        public string CodecName { get; set; }
        public string CodecNameLong { get; set; }
        public string CodecType { get; set; }
        public string CodecTimeBase { get; set; }
        public string CodecTagString { get; set; }
        public string CodecTag { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string SampleAspectRatio { get; set; }
        public string DisplayAspectRation { get; set; }
        public string PixelFormat { get; set; }
        public string FrameRate { get; set; }
        public long FrameCount { get; set; }
        public string SampleRate { get; set; }
        public int Channels { get; set; }
        public string ChannelLayout { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan Duration { get; set; }

        public List<KeyValuePair<string, string>> OtherValues { get; set; }

        public FFmpegStreamInfo()
        {
            OtherValues = new List<KeyValuePair<string, string>>();
        }
    }
}
