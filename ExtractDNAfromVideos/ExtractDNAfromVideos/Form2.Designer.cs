﻿namespace ExtractDNAfromVideos
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.labelOverview = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.lavelTitle = new System.Windows.Forms.Label();
            this.labelFilename = new System.Windows.Forms.Label();
            this.buttonImg = new System.Windows.Forms.Button();
            this.labelImgfilename = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(65, 150);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(389, 47);
            this.textBox1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(220, 208);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "ENTER";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelOverview
            // 
            this.labelOverview.AutoSize = true;
            this.labelOverview.Location = new System.Drawing.Point(63, 135);
            this.labelOverview.Name = "labelOverview";
            this.labelOverview.Size = new System.Drawing.Size(134, 12);
            this.labelOverview.TabIndex = 3;
            this.labelOverview.Text = "input overview of video";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(65, 111);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(389, 21);
            this.textBox2.TabIndex = 4;
            // 
            // lavelTitle
            // 
            this.lavelTitle.AutoSize = true;
            this.lavelTitle.Location = new System.Drawing.Point(63, 96);
            this.lavelTitle.Name = "lavelTitle";
            this.lavelTitle.Size = new System.Drawing.Size(24, 12);
            this.lavelTitle.TabIndex = 5;
            this.lavelTitle.Text = "title";
            //this.lavelTitle.Click += new System.EventHandler(this.label1_Click);
            // 
            // labelFilename
            // 
            this.labelFilename.AutoSize = true;
            this.labelFilename.Location = new System.Drawing.Point(70, 29);
            this.labelFilename.Name = "labelFilename";
            this.labelFilename.Size = new System.Drawing.Size(0, 12);
            this.labelFilename.TabIndex = 6;
            // 
            // buttonImg
            // 
            this.buttonImg.Location = new System.Drawing.Point(65, 54);
            this.buttonImg.Name = "buttonImg";
            this.buttonImg.Size = new System.Drawing.Size(132, 24);
            this.buttonImg.TabIndex = 7;
            this.buttonImg.Text = "choose image";
            this.buttonImg.UseVisualStyleBackColor = true;
            this.buttonImg.Click += new System.EventHandler(this.button2_Click);
            // 
            // labelImgfilename
            // 
            this.labelImgfilename.AutoSize = true;
            this.labelImgfilename.Location = new System.Drawing.Point(210, 62);
            this.labelImgfilename.Name = "labelImgfilename";
            this.labelImgfilename.Size = new System.Drawing.Size(0, 12);
            this.labelImgfilename.TabIndex = 9;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(522, 262);
            this.Controls.Add(this.labelImgfilename);
            this.Controls.Add(this.buttonImg);
            this.Controls.Add(this.labelFilename);
            this.Controls.Add(this.lavelTitle);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.labelOverview);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelOverview;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label lavelTitle;
        private System.Windows.Forms.Label labelFilename;
        private System.Windows.Forms.Button buttonImg;
        private System.Windows.Forms.Label labelImgfilename;
    }
}